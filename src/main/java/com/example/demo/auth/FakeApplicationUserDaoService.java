package com.example.demo.auth;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.example.demo.security.ApplicationUserRole.*;

@Repository("fakeDao")
public class FakeApplicationUserDaoService implements ApplicationUserDao {

	private final PasswordEncoder passwordEncoder;

	@Autowired
	public FakeApplicationUserDaoService(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public Optional<ApplicationUser> selectApplicationUserByUsername(String username) {
		return getApplicationUsers()
			.stream()
			.filter(applicationUser -> username.equals(applicationUser.getUsername()))
			.findFirst();
	}

	private List<ApplicationUser> getApplicationUsers() {
		return Lists.newArrayList(
			new ApplicationUser(
				"midas",
				passwordEncoder.encode("midas"),
				STUDENT.getGrantedAuthorities(),
				true, true,true, true
			),
			new ApplicationUser(
				"ryanjan",
				passwordEncoder.encode("ryanjan"),
				ADMIN.getGrantedAuthorities(),
				true, true,true, true
			),
			new ApplicationUser(
				"thor",
				passwordEncoder.encode("thor"),
				ADMINTRAINEE.getGrantedAuthorities(),
				true, true,true, true
			)
		);
	}

}
